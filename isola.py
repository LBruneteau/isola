#!/usr/bin/python3.8

'''
Jeu de l'isola en python
-------------------------------------------------------------
| contenu de la case | dans la grille | affichage | couleur |
-------------------------------------------------------------
|     Espace vide    |        0       |    ' '    |  blanc  |
|     Pion rouge     |        1       |    'R'    |  rouge  |
|      Pion bleu     |        2       |    'B'    |  bleu   |
|      Obstacle      |        3       |    'X'    |  vert   |
-------------------------------------------------------------
'''

from typing import List, Tuple, Set

try:
    from colorama import Fore, Style #pip install colorama
except ModuleNotFoundError:
    from os import system
    system('pip install --upgrade pip && pip install colorama')
    from colorama import Fore, Style
R, B, W, G, RA = Fore.RED, Fore.BLUE, Fore.WHITE, Fore.GREEN, Style.RESET_ALL

__authors__ = {'Ulysse Poiraud-Castro', 'Louis BRUNETEAU'}
__version__ = '1.0'
__email__ = 'lbruneteau@notre-dame-reze.fr'

def grille_vide() -> List[List[int]]:
    '''
    Retourne une nouvelle grille de jeu
    Les pions sont placés
    '''
    grille: List[List[int]] = [[0] * 7 for _ in range(7)]
    grille[0][3] = 1 #Rouge
    grille[6][3] = 2 #Blanc
    return grille

def str_grille(grille: List[List[int]]) -> str:
    '''
    Retourne une version str de la grille pour etre affichée
    comprend les \n
    affiche les coords autour
    '''
    lettres: str = 'ABCDEFG' #Les lettres sur les cotés (coord)
    trad: str = ' RBX' #Lien entre les nombres de la grille et les lettres
    couleurs: tuple = (W, R, B, G)
    res: str = ' ' * 5
    for num in range(1, 8):
        res += f'{num})  '
    res += f'\n   {"-" * 29}\n'
    for index, ligne in enumerate(grille):
        res += f'{lettres[index]}) |' #num en début de ligne
        for case in ligne:
            res += f' {couleurs[case]}{trad[case]}{RA} |'
        res += f'\n   {"-" * 29}\n'
    return res

def trouver(grille: List[List[int]], bleu: bool) -> Tuple[int, int]:
    '''
    Donne les coordonnées du pion rouge ou bleu
    '''
    chiffre_pion: int = int(bleu) + 1 #1 si rouge 2 si bleu
    for index, ligne in enumerate(grille):
        if chiffre_pion in ligne:
            return (index, ligne.index(chiffre_pion))
    return (-1, -1) #pour mypy


def cases_autour(grille: List[List[int]], bleu: bool) -> Set[Tuple[int, int]]:
    '''
    Retourne les coordonnées des cases autour d'un pion si elles sont accessibles
    '''
    res: Set[Tuple[int, int]] = set()
    pion: Tuple[int, int] = trouver(grille, bleu)
    for y in range(pion[0] -1, pion[0] + 2):
        for x in range(pion[1] -1, pion[1] + 2):
            if all(ligne in range(7) for ligne in (y, x)): #Vérifie si on est dans la grille
                if grille[y][x] == 0:
                    res.add((y, x))
    return res

def bouger(grille: List[List[int]], bleu: bool) -> None:
    '''
    affiche la grille
    Demande a l'utilisateur de choisir une case
    recommence si invalide
    bouge le pion
    '''
    input(f'{B if bleu else R}Au tour du joueur {"bleu" if bleu else "rouge"} : {RA}')
    print(str_grille(grille))
    case = input(f'{B if bleu else R}Sur quelle case voulez-vous aller ? : {RA}').upper()
    valide: bool = False #On part du principe que la case est invalide
    if len(case) == 2:
        lettres: str = 'ABCDEFG' #Lien entre les lettres et les chiffres
        if case[0] in lettres and case[1] in '1234567':
            cases_acc: Set[Tuple[int, int]] = cases_autour(grille, bleu)
            coord: Tuple[int, int] = (lettres.index(case[0]), int(case[1]) - 1)
            if coord in cases_acc:
                valide = True
    
    if valide:
        ancienne_case: Tuple[int, int] = trouver(grille, bleu)
        grille[ancienne_case[0]][ancienne_case[1]] = 0
        grille[coord[0]][coord[1]] = 2 if bleu else 1
    else:
        input(f'{B if bleu else R}Case invalide ! : {RA}')
        bouger(grille, bleu)

def placer_mur(grille: List[List[int]], bleu: bool) -> None:
    '''
    Demande de placer un mur à un endroit vide
    '''
    print(str_grille(grille))
    case: str = input(f'{B if bleu else R}Placez un mur : {RA}').upper()
    valide: bool = False
    if len(case) == 2:
        lettres: str = 'ABCDEFG'
        if case[0] in lettres and case[1] in '1234567':
            coord: Tuple[int, int] = (lettres.index(case[0]), int(case[1]) - 1)
            if grille[coord[0]][coord[1]] == 0:
                valide = True
    
    if valide:
        grille[coord[0]][coord[1]] = 3
    else:
        input(f'{B if bleu else R}Case invalide ! : {RA}')
        placer_mur(grille, bleu)

def jeu() -> int:
    '''
    Joue une partie
    retourne -1 si egalité
    1 si blanc gagnant
    0 si noir gagnant
    '''
    grille: List[List[int]] = grille_vide()
    joueur: bool = True #Passe de True a False a chaque tour correspond a joueur bleu ou non
    while True:
        if len(cases_autour(grille, False)) == len(cases_autour(grille, True)) == 0:
            print(str_grille(grille))
            print('Égalité !')
            return -1
        if len(cases_autour(grille, False)) == 0:
            print(str_grille(grille))
            print(f'{B}Le joueur bleu gagne !{RA}')
            return 1
        if len(cases_autour(grille, True)) == 0:
            print(str_grille(grille))
            print(f'{R}Le joueur rouge gagne !{RA}')
            return 0
        bouger(grille, joueur)
        placer_mur(grille, joueur)
        joueur = not joueur
    return -1 #pour mypy

def main() -> None:
    '''
    Joue des parties en boucle
    '''
    score: List[int] = [0] * 2
    en_route: bool = True
    while en_route:
        gagnant: int = jeu()
        if gagnant >= 0:
            score[gagnant] += 1
        print(f'{R}Rouge : {score[0]}\n{B}Bleu : {score[1]}{RA}')
        en_route = input('Encore une partie ? [o/n] : ').lower() in ('oui', 'o')

if __name__ == "__main__":
    main()
