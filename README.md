# Isola

Le but de ce projet est de reproduire le jeu de l'[isola](https://fr.wikipedia.org/wiki/isola_(Jeu)) avec *Python 3*.  
Il a été réalisé par *Louis BRUNETEAU* et *Ulysse POIRAUD-CASTRO*.  

## Règles du jeu

À chaque tour, chaque joueur :  

1. **Déplace** son pion vers une case adjacente ou en diagonale  
2. **Détruit** une case du jeu non occupée pour le reste de la partie  
**Le premier joueur qui ne peut plus déplacer son pion perd la partie**  

## Comment installer le jeu  

- Méthode 1 : cloner le projet  

```bash
git clone https://gitlab.com/LBruneteau/isola
```

- Méthode 2 : télécharger graphiquement  

1. Cliquez sur l'icone de **téléchargement** dans le coin de la page et choisissez un format  
2. **Décompressez** le fichier obtenu  

Vous devez avoir une version de python supérieure ou égale à **python3.6**  
Installez le module *colorama* avec pip. Si vous ne faites pas le paquet sera installé au lancement du jeu.  

```bash
pip install --upgrade pip
pip install colorama
```

## Comment jouer au jeu  

Lancer le fichier *isola.py* avec python 3  

```bash
python3 chemin/vers/isola.py
```

Une partie débute.  
Le joueur **rouge** est représenté par un **R**  
Le joueur **bleu** est représenté par un **B**  
Les **obstacles** sont représentés par un **X**  
Appuyez sur **Entrée** pour commencer votre tour  
Ensuite entrez les *coordonnées* de la **case** sur laquelle vous voulez aller  
**Les lettres doivent être mises avant les chiffres**  
Entrez ensuite les *coordonnées* du **mur** que vous voulez placer  
À la fin de la partie le score est affiché  
Tapez `o` ou `oui` pour refaire une partie  
